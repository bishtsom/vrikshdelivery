import React from 'react'
import HomeStack from '../DeliveryBoy/src/navigation/MainStackNavigator'

const App = () => {
  console.disableYellowBox = true;
  return <HomeStack />
};

export default App;