import { createStackNavigator } from 'react-navigation-stack';
import { createSwitchNavigator, createAppContainer, } from 'react-navigation';
import DeliveryLogin from '../components/DeliveryLogin'
import HomePage from '../components/HomePage'
import Profile from '../components/Profile'
import OrdersDelivered from '../components/OrdersDelivered'
import OngoingOrders from '../components/OngoingOrders'
import Chat from '../components/Chat'
import SplashScreen from '../components/SplashScreen'
import OrderDeliveredDetails from '../components/OrderDeliveredDetails'
import OngoingOrderDetail from '../components/OngoingOrderDetail'

const HomeStack = createStackNavigator({
  SplashScreen: {screen: SplashScreen,
      navigationOptions: {
        header: null      
      }},
  DeliveryLogin: {screen: DeliveryLogin,
    navigationOptions: {
      header: null      
    }},
  HomePage: {screen: HomePage},
  Profile: {screen: Profile},
  OrdersDelivered: {screen: OrdersDelivered},
  OngoingOrders: {screen: OngoingOrders},
  Chat: {screen: Chat},
  OrderDeliveredDetails: {screen: OrderDeliveredDetails},
  OngoingOrderDetail: {screen: OngoingOrderDetail},
  initialRouteName: "SplashScreen"
})

const MySwitchNavigator = createSwitchNavigator({
  Home: HomeStack,
  initialRouteName: HomeStack
});

export default createAppContainer(MySwitchNavigator);