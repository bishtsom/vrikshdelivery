import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../Utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';

const { width } = Dimensions.get('window')

export default class OngoingOrderDetail extends Component {
    constructor(props) {
        super()
        this.state = {

        }
    }

    render() {
        const { navigation } = this.props;
        const newOrders = navigation.getParam('ongoingOrders');
        var date = new Date(newOrders.complete_date).getDate();
        var month = new Date(newOrders.complete_date).getMonth() + 1;
        var year = new Date(newOrders.complete_date).getFullYear();
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'} style={{ margin: 20 }}>

                        {/* <View style={{ marginBottom: 15, backgroundColor: 'white', borderRadius: 5, padding: 5 }}> */}
                            <Text style={{ marginTop: 10, fontSize: 18, color: Constants.Colors.White, marginBottom: 15 }}>
                                Order Id: {newOrders.order_id}
                            </Text>
                            <Text style={{ fontSize: 18, color: Constants.Colors.White, marginBottom: 15 }}>
                                Customer Name: {newOrders.customer_name}
                            </Text>
                            <Text style={{ fontSize: 18, color: Constants.Colors.White, marginBottom: 15 }}>
                                Delivery Date: {date + '-' + month + '-' + year}
                            </Text>
                            <Text style={{ fontSize: 18, color: Constants.Colors.White, marginBottom: 15 }}>
                                Delivery Address: {newOrders.delivery_address}
                            </Text>
                        {/* </View> */}

                        {newOrders && newOrders.products && newOrders.products.length > 0 && newOrders.products.map((data, key) => {
                            return (
                                <View key={key} style={[styles.productBox, { flexDirection: 'row', justifyContent: 'space-between', flex: 1 }]}>
                                    <Text style={{flex: 6, fontSize: 18, color: Constants.Colors.White }}>{data.product_name}</Text>
                                    <Text style={{flex: 2, fontSize: 18, color: Constants.Colors.White, textAlign:'center' }}>{data.quantity}</Text>
                                    <Text style={{flex: 2, fontSize: 18, color: Constants.Colors.White }}>{'₹'+data.total_price}</Text>
                                </View>
                            )
                        })}

                        {/* <View style={{ marginTop: 15, backgroundColor: 'white', borderRadius: 5, padding: 5 }}> */}
                            <Text style={{ fontSize: 18, color: Constants.Colors.White, marginTop: 20, marginBottom: 15 }}>
                                Bill: {'₹'+newOrders.bill}
                            </Text>
                            <Text style={{ fontSize: 18, color: Constants.Colors.White, marginBottom: 15 }}>
                                Total Bill: {'₹'+newOrders.total_bill}
                            </Text>
                            {/* <Text style={{ fontSize: 18, color: Constants.Colors.White, marginBottom: 15 }}>
                                Payment Method: {newOrders.total_bill}
                            </Text> */}
                        {/* </View> */}
                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    rowContainerStyle: {
        flex: 1,
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "space-between",
    },
    productBox: {
        // alignSelf: 'center',
        alignItems: "center",
        height: 60,
        borderRadius: 5,
        padding: 8,
        color: "white",
        backgroundColor: Constants.Colors.DarkBlue,
        borderColor: Constants.Colors.White,
        borderWidth: 1,
    },
    goToSignInOuter: {
        marginBottom: 20,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        width: 120,
        borderRadius: 3,
        color: "white",
        marginTop: 15,
        borderColor: Constants.Colors.LightBlue,
        borderWidth: 2,
    },
    goToSignInInner: {
        fontSize: 18,
        color: 'white',
        fontStyle: 'normal',
    },
})