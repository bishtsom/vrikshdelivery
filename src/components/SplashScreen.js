import React, { Component } from 'react'
import { ImageBackground, Keyboard, Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import { pushNotificationInit, pushNotificationRemove } from "../Utilities/PushNotification";
import { NavigationActions, StackActions } from "react-navigation"
import AsyncStorage from '@react-native-community/async-storage';

const { width } = Dimensions.get('window')

const resetActionLogin = StackActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'DeliveryLogin' })
    ], key: null
});
const resetActionHome = StackActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'HomePage' })
    ], key: null
});
export default class SplashScreen extends Component {
    constructor(props) {
        super()
        this.state = {
        }
    }

    componentWillMount() {
        pushNotificationInit();
    }

    componentDidMount() {
        setTimeout(() => {
            AsyncStorage.getItem("token").then((tokenValue) => {
                if (tokenValue == null || tokenValue == '') {
                    this.props.navigation.dispatch(resetActionLogin)
                } else {
                    this.props.navigation.dispatch(resetActionHome)
                }
            })
        }, 3000);
    }

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <ImageBackground
                    style={{ height: width - 20, width: width, justifyContent: 'center' }}
                    source={require('../assets/images/logo.png')}>
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
})