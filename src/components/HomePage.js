import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../Utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationActions, StackActions } from "react-navigation"

const { width } = Dimensions.get('window')
const resetActionLogin = StackActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'DeliveryLogin' })
    ], key: null
});
export default class HomePage extends Component {
    constructor(props) {
        super()
        this.state = {
            name: '',
            email: '',
            contact: '',
            outletName: '',
            outletAddress: ''
        }
    }

    logOut() {
        AsyncStorage.clear();
        this.props.navigation.dispatch(resetActionLogin)
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'}>
                        <View style={styles.logo} />

                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', margin: 20 }}>
                            <TouchableOpacity onPress={() => navigate('Profile')} style={[styles.productBox, { borderRadius: 8, height: width / 2.8, width: width / 2.8, backgroundColor: Constants.Colors.Orange, alignItems: 'center', justifyContent: 'center' }]}>
                                <Text style={{ textAlign: 'center', textAlignVertical: 'center', color: 'white', fontSize: 18, fontWeight: '600' }}>Profile</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => navigate('OrdersDelivered')} style={[styles.productBox,{ borderRadius: 8, height: width / 2.8, width: width / 2.8, backgroundColor: Constants.Colors.Orange, alignItems: 'center', justifyContent: 'center' }]}>
                                <Text style={{ textAlign: 'center', textAlignVertical: 'center', color: 'white', fontSize: 18, fontWeight: '600' }}>Orders {"\n"}Delivered</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', margin: 20 }}>
                            <TouchableOpacity onPress={() => navigate('OngoingOrders')} style={[styles.productBox,{ borderRadius: 8, height: width / 2.8, width: width / 2.8, backgroundColor: Constants.Colors.newRed, alignItems: 'center', justifyContent: 'center' }]}>
                                <Text style={{ textAlign: 'center', textAlignVertical: 'center', color: 'white', fontSize: 18, fontWeight: '600' }}>Ongoing {"\n"}Orders</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.logOut()} style={[styles.productBox,{ borderRadius: 8, height: width / 2.8, width: width / 2.8, backgroundColor: Constants.Colors.newRed, alignItems: 'center', justifyContent: 'center' }]}>
                                <Text style={{ textAlign: 'center', textAlignVertical: 'center', color: 'white', fontSize: 18, fontWeight: '600' }}>Logout</Text>
                            </TouchableOpacity>
                        </View>

                        {/* <SubmitButton
                            onPress={() => this.logOut()}
                            text="Logout"
                            style={{ alignSelf: 'flex-end' }}
                            backGroundStyle={{ width: width / 4 }}
                        /> */}
                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 20,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    productBox: {
        // alignSelf: 'center',
        borderRadius: 10,
        padding: 8,
        backgroundColor: Constants.Colors.White,
        shadowColor: Constants.Colors.Black,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 0.3,
        elevation: 3
    },
})