import React, { Component } from 'react'
import { Keyboard, TouchableHighlight, Modal, Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../Utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import AsyncStorage from '@react-native-community/async-storage';
import RestClient from '../Utilities/RestClient';
import Connection from "../Utilities/Connection";
import ImagePickerCropper from "react-native-image-crop-picker";
import Regex from '../Utilities/Regex';

import _ from "lodash";

const { width } = Dimensions.get('window')

export default class Profile extends Component {
    constructor(props) {
        super()
        this.state = {
            name: '',
            age: '',
            email: '',
            contact: '',
            vehicleNumber: '',
            vehicleType: '',
            tokenValue: '',
            profileImg: '',
            ImagemodalVisible: false,
            profilePic: {},
            ProfileImageSource: '',
            imageType: '',
        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    openImagePickerCropper = (imageType) => {
        ImagePickerCropper.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            this.setState({ ImagemodalVisible: false })
            let source = { uri: image.path };
            let fileName = image.path.split("/")
            let len = fileName.length;
            let name = fileName[len - 1]
            if (this.state.imageType == 'profile') {
                this.setState({
                    ProfileImageSource: source,
                    profilePic: {
                        uri: image.path,
                        name: name,
                        filename: name,
                        type: image.mime
                    }
                })
                 this.saveProfileImage();
            }
        })
    }

    openImagePickerCropperCamera = (imageType) => {
        ImagePickerCropper.openCamera({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            this.setState({ ImagemodalVisible: false })
            let source = { uri: image.path };
            let fileName = image.path.split("/")
            let len = fileName.length;
            let name = fileName[len - 1]
            if (this.state.imageType == 'profile') {

                this.setState({
                    ProfileImageSource: source,
                    profilePic: {
                        uri: image.path,
                        name: name,
                        filename: name,
                        type: image.mime
                    }
                })
                 this.saveProfileImage();
            }
        })
    }

    saveProfileImage() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            const body = new FormData();
            body.append("file", {uri: this.state.profilePic.uri})
            RestClient.imageUpload("api/delivery_boy/profile/update/prof_pic", body, tokenValue).then((result) => {
                this.setState({ profileImg: this.state.profilePic.uri })
                console.log("image", result)
            }).catch((error) => {
                console.log("error in drawer", error)
            })
        })
    }

    componentDidMount() {
        this.getNewDetails();
    }

    getNewDetails() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("token value", tokenValue)
            RestClient.get("api/delivery_boy/profile", {}, tokenValue).then((result) => {
                console.log("profile new:", JSON.stringify(result));
                if (result.success == "1") {
                    this.setState({ name: result.driver_details.name, age: result.driver_details.age, email: result.driver_details.email, contact: result.driver_details.contact, vehicleNumber: result.driver_details.vehicle_number, vehicleType: result.driver_details.vehicle_type })
                } else {
                    alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        })
    }

    changeEmail() {
        Keyboard.dismiss()
        let { email } = this.state;

        if (_.isEmpty(email && email.trim())) {
            alert('Please enter your email');
            return;
        }

        if (!Regex.validateEmail(email && email.trim())) {
            alert('Enter a valid email');
            return;
        }

        RestClient.put("api/delivery_boy/profile/update/email", { email: email }, this.state.tokenValue).then((result) => {
            console.log("result", JSON.stringify(result))
            if (result.success == "1") {
                alert(result.message)
                this.props.navigation.goBack();
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'}>

                        {/* <TouchableOpacity style={[styles.navIcons, { marginLeft: 20, marginTop: 20, }]} onPress={() => this.setState({ ImagemodalVisible: true, imageType: 'profile' })}>
                            <Image source={{ uri: `${Connection.getBaseUrl()}/${this.state.profileImg}` }}
                                style={styles.navIcons} resizeMode={'contain'} />
                        </TouchableOpacity> */}

                        <FormTextInput
                            autoFocus={false}
                            ref='phone'
                            fontSize={16}
                            placeHolderText='Name'
                            placeHolderColor={Constants.Colors.WhiteUpd}
                            returnKey='done'
                            secureText={false}
                            editable={false}
                            isPassword={false}
                            showPassword={false}
                            value={this.state.name}
                            onChangeText={(name) => this.setState({ name })}
                            textColor={Constants.Colors.White}
                            style={{ marginVertical: 5 }}
                        />

                        <FormTextInput
                            autoFocus={false}
                            ref='age'
                            value={this.state.age}
                            fontSize={16}
                            placeHolderText='Age'
                            placeHolderColor={Constants.Colors.WhiteUpd}
                            returnKey='done'
                            secureText={false}
                            editable={false}
                            isPassword={false}
                            showPassword={false}
                            onChangeText={(age) => this.setState({ age })}
                            textColor={Constants.Colors.White}
                            style={{ marginVertical: 5 }}
                        />

                        <FormTextInput
                            autoFocus={false}
                            ref='email'
                            value={this.state.email}
                            fontSize={16}
                            placeHolderText='Email'
                            placeHolderColor={Constants.Colors.WhiteUpd}
                            returnKey='done'
                            secureText={false}
                            isPassword={false}
                            showPassword={false}
                            onChangeText={(email) => this.setState({ email })}
                            textColor={Constants.Colors.White}
                            style={{ marginVertical: 5 }}
                        />

                        <SubmitButton
                            onPress={() => this.changeEmail()}
                            text="Update"
                            style={{ alignSelf: 'flex-end', marginTop: 0 }}
                            backGroundStyle={{ width: width / 4, padding: 8 }}
                        />

                        <FormTextInput
                            autoFocus={false}
                            ref='contact'
                            fontSize={16}
                            value={this.state.contact}
                            placeHolderText='Contact'
                            placeHolderColor={Constants.Colors.WhiteUpd}
                            returnKey='done'
                            editable={false}
                            secureText={false}
                            isPassword={false}
                            showPassword={false}
                            onChangeText={(contact) => this.setState({ contact })}
                            textColor={Constants.Colors.White}
                            style={{ marginVertical: 5 }}
                        />

                        <FormTextInput
                            autoFocus={false}
                            ref='vehicleNumber'
                            value={this.state.vehicleNumber}
                            fontSize={16}
                            placeHolderText='Vehicle Number'
                            placeHolderColor={Constants.Colors.WhiteUpd}
                            returnKey='done'
                            editable={false}
                            secureText={false}
                            isPassword={false}
                            showPassword={false}
                            onChangeText={(vehicleNumber) => this.setState({ vehicleNumber })}
                            textColor={Constants.Colors.White}
                            style={{ marginVertical: 5 }}
                        />

                        <FormTextInput
                            autoFocus={false}
                            ref='vehicleType'
                            fontSize={16}
                            placeHolderText='Vehicle Type'
                            editable={false}
                            value={this.state.vehicleType}
                            placeHolderColor={Constants.Colors.WhiteUpd}
                            returnKey='done'
                            secureText={false}
                            isPassword={false}
                            showPassword={false}
                            onChangeText={(vehicleType) => this.setState({ vehicleType })}
                            textColor={Constants.Colors.White}
                            style={{ marginVertical: 5 }}
                        />

                        <Modal animationType={"slide"} transparent={true}
                            visible={this.state.ImagemodalVisible}
                            onRequestClose={() => { console.log("Modal has been closed.") }}>
                            <View style={{ flex: 8 }}></View>
                            <View style={{ flex: 2, backgroundColor: "white" }}>
                                <TouchableHighlight
                                    style={{ flex: 1, borderBottomWidth: 1, borderColor: "gray", justifyContent: "center" }}
                                    onPress={() => {
                                        this.openImagePickerCropper(this.state.imageType)
                                    }}>
                                    <Text style={styles.text}>Select From Gallery..</Text>
                                </TouchableHighlight>

                                <TouchableHighlight
                                    style={{ flex: 1, borderBottomWidth: 1, borderColor: "gray", justifyContent: "center" }}
                                    onPress={() => {
                                        this.openImagePickerCropperCamera(this.state.imageType)
                                    }}>
                                    <Text style={styles.text}>Open Camera..</Text>
                                </TouchableHighlight>

                                <TouchableHighlight
                                    style={{ flex: 1, borderBottomWidth: 1, borderColor: "gray", justifyContent: "center" }}
                                    onPress={() => { this.setState({ ImagemodalVisible: !this.state.ImagemodalVisible }) }}>
                                    <Text style={styles.text}>Cancel</Text>
                                </TouchableHighlight>
                            </View>
                        </Modal>
                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 20,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    navIcons: {
        height: 100,
        width: 100,
        //borderColor: Constants.Colors.White,
        //borderWidth: 2,
        //backgroundColor: Constants.Colors.White,
        borderRadius: 100 / 2
    },
    text: {
        fontSize: 18,
        alignSelf: "center"
    },
})