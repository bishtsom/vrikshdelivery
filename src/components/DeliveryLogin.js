import React, { Component } from 'react'
import { Keyboard, Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../Utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import Regex from '../Utilities/Regex';
import _ from "lodash";
import RestClient from '../Utilities/RestClient';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationActions, StackActions } from "react-navigation"

const { width } = Dimensions.get('window')

const resetActionLogin = StackActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'HomePage' })
    ], key: null
});
export default class DeliveryLogin extends Component {
    constructor(props) {
        super()
        this.state = {
            phone: '',
            password: '',
            //phone: '1111111111',
            //password: '85384',
        }
    }

    componentDidMount() {
    }

    loginUser() {
        Keyboard.dismiss()
        let { phone, password } = this.state;

        if (_.isEmpty(phone && phone.trim())) {
            alert('Please enter your phone');
            return;
        }

        if (_.isEmpty(password)) {
            alert('Please enter your password');
            return;
        }

        // AsyncStorage.getItem("pushToken").then((pushToken) => {
        console.log("push token are", Constants.registrationId)
        RestClient.post("login/delivery_boy", { contact: this.state.phone, password: this.state.password, registration_id: Constants.registrationId }).then((result) => {
            console.log("result", JSON.stringify(result))
            if (result.success == "1") {
                AsyncStorage.setItem("token", result.token)
                console.log("token", JSON.stringify(result.token))
                this.props.navigation.dispatch(resetActionLogin)
            } else {
                alert(result.message)
            }
        }).catch(error => {
            alert(error)
        });
        // })
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'}>

                        <View style={{ justifyContent: 'center', alignSelf: 'center' }}>
                            <View style={{ height: 30 }} />

                            <Image source={Constants.Images.splash} style={styles.logo} resizeMode={'contain'} />
                            <FormTextInput
                                autoFocus={false}
                                ref='phone'
                                fontSize={20}
                                placeHolderText='Phone No.'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                returnKey='done'
                                secureText={false}
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(phone) => this.setState({ phone })}
                                textColor={Constants.Colors.White}
                                fontSize={16}
                            />

                            <FormTextInput
                                autoFocus={false}
                                ref='password'
                                fontSize={20}
                                placeHolderText='Password'
                                placeHolderColor={Constants.Colors.WhiteUpd}
                                returnKey='done'
                                secureText={true}
                                isPassword={false}
                                showPassword={false}
                                onChangeText={(password) => this.setState({ password })}
                                textColor={Constants.Colors.White}
                                fontSize={16}
                            />

                            <SubmitButton
                                onPress={() => this.loginUser()}
                                //onPress={() => this.props.navigation.navigate("HomePage")}
                                text="Login"
                            />
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignSelf: 'center',
        justifyContent: 'center'
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center',
        marginTop: 40,
        marginBottom: 20
    },
    register: {
        fontSize: 16,
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
})