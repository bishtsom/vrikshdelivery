import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, ScrollView, KeyboardAvoidingView, SafeAreaView, TouchableOpacity } from 'react-native'
import Constants from "../Utilities/Constants";
import FormTextInput from "../common/FormTextInput";
import SubmitButton from "../common/FormSubmitButton";
import Background from '../common/Background';
import RestClient from '../Utilities/RestClient';
import AsyncStorage from '@react-native-community/async-storage';

const { width } = Dimensions.get('window')

export default class OngoingOrderDetail extends Component {
    constructor(props) {
        super()
        this.map = null;
        this.state = {
            tokenValue: '',

        }
    }

    componentWillMount() {
        AsyncStorage.getItem("token").then((tokenValue) => {
            console.log("tokkn are", tokenValue)
            this.setState({ tokenValue: tokenValue })
        })
    }

    completeOrder(pickupStatus, orderId) {
        if (pickupStatus) {
            RestClient.queryPost("api/delivery_boy/order/complete", { order_id: orderId, status: 1 }, this.state.tokenValue).then((result) => {
                console.log("result", JSON.stringify(result))
                if (result.success == "1") {
                    alert(result.message)
                    this.props.navigation.goBack();
                } else {
                    alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        } else {
            RestClient.putQuery("api/delivery_boy/order/pickup", { order_id: orderId }, this.state.tokenValue).then((result) => {
                console.log("result", JSON.stringify(result))
                if (result.success == "1") {
                    alert(result.message)
                    this.props.navigation.goBack();
                } else {
                    alert(result.message)
                }
            }).catch(error => {
                alert(error)
            });
        }
    }

    render() {
        const { navigation } = this.props;
        const newOrders = navigation.getParam('ongoingOrders');
        return (
            <Background style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'} keyboardShouldPersistTaps="always" keyboardDismissMode={(Platform.OS === 'ios') ? 'on-drag' : 'interactive'}>
                    <KeyboardAvoidingView behavior={'position'} style={{ margin: 20 }}>

                        {/* <View style={{ backgroundColor: 'white', borderRadius: 5, padding: 5 }}> */}
                            <Text style={{ marginTop: 10, fontSize: 18, color: Constants.Colors.White, marginBottom: 15 }}>
                                Order Id: {newOrders.order_id}
                            </Text>
                            <Text style={{ fontSize: 18, color: Constants.Colors.White, marginBottom: 15 }}>
                                Customer Name: {newOrders.customer_name}
                            </Text>
                            <Text style={{ fontSize: 18, color: Constants.Colors.White, marginBottom: 15 }}>
                                Delivery Address: {newOrders.delivery_address}
                            </Text>

                            {/* <SubmitButton
                                style={{ marginTop: 5, marginBottom: 5 }}
                                text="Call Customer"
                            /> */}
                        {/* </View> */}

                        {newOrders && newOrders.products && newOrders.products.length > 0 && newOrders.products.map((data, key) => {
                            return (
                                <View key={key} style={[styles.productBox, { flexDirection: 'row', justifyContent: 'space-between', flex: 1 }]}>
                                    <Text style={{flex: 6, fontSize: 18, color: Constants.Colors.White }}>{data.product_name}</Text>
                                    <Text style={{flex: 2, fontSize: 18, color: Constants.Colors.White, textAlign:'center' }}>{data.quantity}</Text>
                                    <Text style={{flex: 2, fontSize: 18, color: Constants.Colors.White }}>{'₹'+data.total_price}</Text>
                                </View>
                            )
                        })}

                        <SubmitButton
                            style={{ marginTop: 25, marginBottom: 5 }}
                            onPress={() => this.completeOrder(newOrders.pickup_status, newOrders.order_id)}
                            text={newOrders.pickup_status ? 'Delivered' : 'Pick up'}
                        />
                    </KeyboardAvoidingView>
                </ScrollView>
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 60,
        alignSelf: 'center'
    },
    register: {
        fontSize: 16,
        fontWeight: '900',
        backgroundColor: 'transparent',
        color: Constants.Colors.WhiteUpd,
        textAlign: 'center',
        marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2
    },
    rowContainerStyle: {
        flex: 1,
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "space-between",
    },
    productBox: {
        alignItems: "center",
        height: 60,
        borderRadius: 5,
        padding: 8,
        color: "white",
        backgroundColor: Constants.Colors.DarkBlue,
        borderColor: Constants.Colors.White,
        borderWidth: 1,
    },
    goToSignInOuter: {
        marginBottom: 20,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        width: 120,
        borderRadius: 3,
        color: "white",
        marginTop: 15,
        borderColor: Constants.Colors.LightBlue,
        borderWidth: 2,
    },
    goToSignInInner: {
        fontSize: 18,
        color: 'white',
        fontStyle: 'normal',
    },
})